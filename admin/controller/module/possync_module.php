<?php
################################################################################################
#  Admin Controller for the POS-Sync inventory syncing plugin - https://www.pos-sync.com       #
#  Copyright 2013, Nahnay, LLC.                                                                #
#  Based on DIY Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com #
################################################################################################
class ControllerModulePOSSyncModule extends Controller {
	
	private $error = array(); // error 
	
	public function index() {   
		//Load the language file for this module
		$this->load->language('module/possync_module');

		//Set the title from the language file $_['heading_title'] string
		$this->document->setTitle($this->language->get('heading_title'));
		
		//Load the settings model. You can also add any other models you want to load here.
		$this->load->model('setting/setting');

		// Check for an API key, if none available, generate one
		if (count($this->model_setting_setting->getSetting('possync','apikey'))<1){
			$tmpapikey = $this->possync_generateApiKey();
			$a = array('possync'=>$tmpapikey);
			$this->model_setting_setting->editSetting('possync', $a);

		}
		$apikey = '';
		foreach ($this->model_setting_setting->getSetting('possync','apikey') as $key => $value) {
		 	$apikey = $value;
		 	$this->data['apikey'] = $value;
		}

		$this->data['apikey_label'] = $this->language->get('apikey_label');
		
		$text_strings = array(
				'heading_title',
				'text_enabled',
				'text_disabled',
				'text_content_top',
				'text_content_bottom',
				'text_column_left',
				'text_column_right',
				'entry_layout',
				'entry_limit',
				'entry_image',
				'entry_position',
				'entry_status',
				'entry_sort_order',
				'button_save',
				'button_cancel',
				'button_add_module',
				'button_remove',
				'entry_example' //this is an example extra field added
		);
		
		foreach ($text_strings as $text) {
			$this->data[$text] = $this->language->get($text);
		}
		//END LANGUAGE
		
		$config_data = array(
				'possync_module_example' //this becomes available in our view by the foreach loop just below.
		);
		
		foreach ($config_data as $conf) {
			if (isset($this->request->post[$conf])) {
				$this->data[$conf] = $this->request->post[$conf];
			} else {
				$this->data[$conf] = $this->config->get($conf);
			}
		}
	
		//This creates an error message. The error['warning'] variable is set by the call to function validate() in this controller (below)
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		//SET UP BREADCRUMB TRAIL. YOU WILL NOT NEED TO MODIFY THIS UNLESS YOU CHANGE YOUR MODULE NAME.
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/possync_module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/possync_module', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

	
		//This code handles the situation where you have multiple instances of this module, for different layouts.
		$this->data['modules'] = array();
		
		if (isset($this->request->post['possync_module_module'])) {
			$this->data['modules'] = $this->request->post['possync_module_module'];
		} elseif ($this->config->get('possync_module_module')) { 
			$this->data['modules'] = $this->config->get('possync_module_module');
		}		

		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		//Choose which template file will be used to display this request.
		$this->template = 'module/possync_module.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);

		//Send the output.
		$this->response->setOutput($this->render());
	}
	
	/*
	 * 
	 * This function is called to ensure that the settings chosen by the admin user are allowed/valid.
	 * You can add checks in here of your own.
	 * 
	 */
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/possync_module')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}



	/*
	 * POS-Sync Functions
	 */

	// Function to generate and save API key
	function possync_generateApiKey(){
	    return  hash('sha256',get_current_user() . "\0" . microtime());
	}

	// Checks to see if the page is called from POS-Sync
	function isPOSSync(){
	    if (isset($_GET['possyncapi'])){
	        $apikey = $_GET['possyncapi'];
	        return possync_validateAPIKey($apikey);
	    }
	    return false;
	}

	// Function to validate api key
	function possync_validateAPIKey($apikey){
	    if (!$apikey==get_option('possync_apikey')){
	        return false;
	    }
	    return true;
	}



}
?>