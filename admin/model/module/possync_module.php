<?php
################################################################################################
#  Admin Model for the POS-Sync inventory syncing plugin - https://www.pos-sync.com            #
#  Copyright 2013, Nahnay, LLC.                                                                #
#  Based on DIY Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com #
################################################################################################
class ModelModulePOSSyncModule extends Model {
	// POSSync inventory syncing plugin does not require a model
	public function createModuleTables() {
	}	
}
?>