<?php
################################################################################################
#  Admin Language module for the POS-Sync inventory syncing plugin - https://www.pos-sync.com  #
#  Copyright 2013, Nahnay, LLC.                                                                #
#  Based on DIY Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com #
################################################################################################

$_['api_version'] = 0.0.2
// Example field added (see related part in admin/controller/module/my_module.php)
$_['apikey_label'] = 'API key to use in at <a href="https://www.pos-sync.com/account">https://www.pos-sync.com/account</a>';



// Heading Goes here:
$_['heading_title']    = 'POS-Sync Module';


// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified module POS-Sync Module!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_example']       = 'Example Entry:'; // this will be pulled through to the controller, then made available to be displayed in the view.
$_['entry_image']        = 'Image (WxH):';
$_['entry_limit']        = 'Limit:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module POS-Sync Module!';
?>