<?php
################################################################################################
#  Catalog Language file for the POS-Sync inventory syncing plugin - https://www.pos-sync.com  #
#  Copyright 2013, Nahnay, LLC.                                                                #
#  Based on DIY Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com #
################################################################################################

// Heading 
$_['heading_title']  = 'POS-Sync Inventory Syncing Module for OpenCart';

?>