<?php
################################################################################################
#  Catalog Model for the POS-Sync inventory syncing plugin - https://www.pos-sync.com          #
#  Copyright 2013, Nahnay, LLC.                                                                #
#  Based on DIY Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com #
################################################################################################
class ModelModulePossync extends Model {
	
	public function getProduct($product_id, $sku){
		$sql = "SELECT p.product_id as id, pd.name, p.quantity, p.date_modified, p.sku  FROM " . DB_PREFIX . "product p WHERE p.product_id = '". $product_id ."'";
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
		$this->log->write($sql);

		$query = $this->db->query($sql);
		var_dump($query);
		$results = array();

		foreach ($query->rows as $product){
			if (isset($product['sku']) && $product['sku']==$sku){
				return $product;
			}
			if (isset($sku)){
				foreach ($this->getSubSKus($product) as $subsku){
					if (isset($subsku['sku']) && $subsku['sku']==$sku){
						return $subsku;
					}
				}
			}else{
				return $this->getSubSKus($product);
			}
		}
		return null;
	}

	protected function getSubSKus($product){
		// get sub skus
		$options = $this->getProductOptions($product['id']);
		$subskus = array();
		foreach ($options as $option){
			if (isset($option['product_option_value'])){
				foreach($option['product_option_value'] as $optionvalue){
					$opt = array(
						"id"       => $product['id'],
						"name"     => $product['name'],
						"sku"      => $optionvalue['sku'],
						"quantity" => $optionvalue['quantity']
					);
					array_push($subskus, $opt);
				}
			}
		}
		return $subskus;		
	}

	public function updateQuantity($product_id, $sku, $qty){
		$this->log->write('updateQuantity');
		// first check if this sku is an option sku
		$options = $this->getProductOptions($product_id);
		foreach ($options as $option){
			if (isset($option['product_option_value'])){
				foreach ($option['product_option_value'] as $option_value){
					if (isset($option_value['sku']) && $option_value['sku']==$sku){
						// update this sku and return
						$this->log->write('updating subsku '. $sku .' with qty '. $qty);
						$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = ". (int)$qty ." WHERE product_option_value_id = '" . (int)$option_value['product_option_value_id'] . "'");
						return true;
					}
				}
			}
		}

		// If we get here we are just updating the top level product
		$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = ". (int)$qty . " WHERE product_id = '" . (int)$product_id . "'");
		return true;
		
	}

	public function getProducts($data=array()){
		$this->log->write('getProducts');
		// Using this getProducts instead of catalog/model because it returnes quantity
		$sql = "SELECT p.product_id as id, pd.name, p.quantity, p.date_modified, p.sku  FROM " . DB_PREFIX . "product p ";
		$sql .= "LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

		if (isset($data['since'])){
			$sql .= " WHERE p.date_modified > '" . $data['since'] . "'";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$product_data = array();
		$this->log->write($sql);
				
		$query = $this->db->query($sql);
	
		foreach ($query->rows as $result) {
			$this->log->write($result['sku'] ."-". $result['id']);
			if (isset($result['sku']) && strlen($result['sku'])>0){
				array_push($product_data, $result);
			}

			foreach($this->getSubSKus($result) as $subsku){
				array_push($product_data, $subsku);
			}

		}		

		return $product_data;
	}


	public function getOrders($data=array()){
		// getOrders
		$sql = "SELECT o.order_id, CONCAT(o.firstname, ' ', o.lastname) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified FROM `" . DB_PREFIX . "order` o";

		if (isset($data["filter_date_modified"])){
			$sql .= " WHERE o.date_modified > STR_TO_DATE('" . $data["filter_date_modified"] . "', '%Y-%m-%d %H:%i:%s')";
		}
		$order_data = array();
				
		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			array_push($order_data, $this->getOrder($result['order_id']));
		}
		return $order_data;
	}

	// modified from opencart /admin/controller/sale/order.php
	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$reward = 0;

			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

			foreach ($order_product_query->rows as $product) {
				$reward += $product['reward'];
			}			

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			$shipping = array(
				'firstname'      => $order_query->row['shipping_firstname'],
				'lastname'       => $order_query->row['shipping_lastname'],
				'company'        => $order_query->row['shipping_company'],
				'address_1'      => $order_query->row['shipping_address_1'],
				'address_2'      => $order_query->row['shipping_address_2'],
				'postcode'       => $order_query->row['shipping_postcode'],
				'city'           => $order_query->row['shipping_city'],
				'zone_id'        => $order_query->row['shipping_zone_id'],
				'zone'           => $order_query->row['shipping_zone'],
				'zone_code'      => $shipping_zone_code,
				'country_id'     => $order_query->row['shipping_country_id'],
				'country'        => $order_query->row['shipping_country'],
				'iso_code_2'     => $shipping_iso_code_2,
				'iso_code_3'     => $shipping_iso_code_3,
				'address_format' => $order_query->row['shipping_address_format'],
				'method'         => $order_query->row['shipping_method'],
				'code'           => $order_query->row['shipping_code']
			);

			$billing = array(
				'firstname'       => $order_query->row['payment_firstname'],
				'lastname'        => $order_query->row['payment_lastname'],
				'company'         => $order_query->row['payment_company'],
				'address_1'       => $order_query->row['payment_address_1'],
				'address_2'       => $order_query->row['payment_address_2'],
				'postcode'        => $order_query->row['payment_postcode'],
				'city'            => $order_query->row['payment_city'],
				'zone_id'         => $order_query->row['payment_zone_id'],
				'zone'            => $order_query->row['payment_zone'],
				'zone_code'       => $payment_zone_code,
				'country_id'      => $order_query->row['payment_country_id'],
				'country'         => $order_query->row['payment_country'],
				'iso_code_2'      => $payment_iso_code_2,
				'iso_code_3'      => $payment_iso_code_3,
				'address_format'  => $order_query->row['payment_address_format']
			);

			$entries = $this->getOrderProducts($order_id);

			$totals = $this->getOrderTotals($order_id);

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'customer'                => $order_query->row['customer'],
				'customer_group_id'       => $order_query->row['customer_group_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'telephone'               => $order_query->row['telephone'],
				'email'                   => $order_query->row['email'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'reward'                  => $reward,
				'order_status_id'         => $order_query->row['order_status_id'],
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'], 
				'user_agent'              => $order_query->row['user_agent'],	
				'accept_language'         => $order_query->row['accept_language'],					
				'date_added'              => $order_query->row['date_added'],
				'date_modified'           => $order_query->row['date_modified'],
				'billing_address'         => $billing,
				'shipping_address'        => $shipping,
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],				
				'entries'                 => $entries,
				'sub_total'               => $totals['sub_total'],
				'total_tax'               => $totals['tax'],
				'voucher'                 => $totals['voucher'],
				'coupon'                  => $totals['coupon'],
				'shipping_cost'           => $totals['shipping']

			);
		} else {
			return false;
		}
	}


	/*
	 * Gets order totals (subtotals, tax, etc.)
 	*/
 	public function getOrderTotals($order_id){
 		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "'");

 		$results = array(
 			'tax'	    => 0,
 			'sub_total' => 0,
 			'total'     => 0,
 			'shipping'  => 0,
 			'voucher'   => 0,
 			'coupon'    => 0

		);
 		foreach($query->rows as $total){
 			$results[$total['code']] += $total['value'];
 		}

 		return $results;
 	}


	/*
	* Gets the product entries for an order minding subskus if present
	*/
	public function getOrderProducts($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

		$results = array();
		foreach ($query->rows as $order_product){
			$sku = null;
			// See if there is option data for this order product
			$subquery = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = ". $order_product['order_product_id']);
			foreach($subquery->rows as $option_data)			{
				if (isset($option_data['sku'])){
					$sku = $option_data['sku'];
				}
			}

			$product = $order_product;
			$product['sku'] = $sku;
			$this->log->write($product);
			array_push($results, $product);
		}

		return $results;
	}


	public function getProductCount(){
		$query = $this->db->query("SELECT COUNT(product_id) FROM " . DB_PREFIX . "product");
		$count = 0;
		foreach ($query->rows as $result) {
			$count = $result;
		}
		return $count;
	}


	// based on opencart coreadmin/model/catalog/product.php
	public function getProductOptions($product_id) {
		$product_option_data = array();

		$product_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();	

			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . (int)$product_option['product_option_id'] . "'");

			foreach ($product_option_value_query->rows as $product_option_value) {
				if (isset($product_option_value['sku'])){
					$product_option_value_data[] = array(
						'product_option_value_id' => $product_option_value['product_option_value_id'],
						'sku'                     => $product_option_value['sku'],
						'option_value_id'         => $product_option_value['option_value_id'],
						'quantity'                => $product_option_value['quantity'],
						'subtract'                => $product_option_value['subtract'],
						'price'                   => $product_option_value['price'],
						'price_prefix'            => $product_option_value['price_prefix'],
						'points'                  => $product_option_value['points'],
						'points_prefix'           => $product_option_value['points_prefix'],						
						'weight'                  => $product_option_value['weight'],
						'weight_prefix'           => $product_option_value['weight_prefix']					
					);
				}
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],				
				//'value'                => $product_option['value'],
				'required'             => $product_option['required']				
			);
		}

		return $product_option_data;
	}
}

?>