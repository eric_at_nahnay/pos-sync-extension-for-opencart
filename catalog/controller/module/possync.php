<?php
################################################################################################
#  Catalog Controller for the POS-Sync inventory syncing plugin - https://www.pos-sync.com     #
#  Copyright 2013, Nahnay, LLC.                                                                #
#  Based on DIY Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com #
################################################################################################
?><?php

class ControllerModulePossync extends Controller {
	protected function index() {
		//noop	
	}

	protected function validateAccess(){
		$apikey='';
		if (isset($_SERVER['HTTP_APIKEY'])){
			$apikey = $_SERVER['HTTP_APIKEY'];
		}
		$status = false;
		if (strlen($apikey)>0){
			$this->load->model('setting/setting');
			$savedApiKey = $this->model_setting_setting->getSetting('possync','apikey');
			if ($apikey==$savedApiKey['possync']){
				$status = true;
			}
		}
		if ($status==false){
			$this->response->setOutput('');
			exit;
		}
		return $status;
	}

	public function products(){
		$this->log->write("Getting products");

		$this->validateAccess();
		$start = 0;
		$limit = 50;
		$since = null;
		if (isset($this->request->get['start'])){
			try{$start = intval($this->request->get['start']);}
			catch(Exception $e){}
		}
		if (isset($this->request->get['limit'])){
			try{$limit = intval($this->request->get['limit']);}
			catch(Exception $e){}
		}
		if (isset($this->request->get['since'])){
			try{$since = $this->request->get['since'];}
			catch(Exception $e){}
		}
		$data=array();
		$data['start'] = $start;
		$data['limit'] = $limit;
		$data['since'] = $since;
		$this->load->model('module/possync');
		$products_ = $this->model_module_possync->getProducts($data);
		$this->response->setOutput(json_encode($products_));
	}

	public function product(){
		$this->log->write('Getting product...');
		$this->validateAccess();
		$this->load->model('module/possync');
		$id = -1;
		$sku = null;
		if (isset($this->request->get['id'])){
			try{$id = intval($this->request->get['id']);}
			catch(Exception $e){}
		}
		if (isset($this->request->get['sku'])){
			try{$sku = intval($this->request->get['sku']);}
			catch(Exception $e){}
		}
		$this->log->write('id '. $id .' sku '. $sku);
		$product = $this->model_module_possync->getProduct($id, $sku);
		$this->log->write('Got back from getting product');
		$this->log->write($product);
		$product_ = array();
		if ($product) {
			$product_['id'] = $product['product_id'];
			$product_['name'] = $product['name'];
			$product_['sku'] = $product['sku'];
			$product_['quantity'] = $product['quantity'];
		}
		$this->response->setOutput(json_encode($product_));
	}

	public function getProductCount(){
		$this->validateAccess();
		$this->load->model('module/possync');
		$this->response->setOutput(json_encode($this->model_module_possync->getProductCount()));
	}

	public function orders(){
		$this->validateAccess();
		
		$since = "1970-01-01 00:00:00";
		if (isset($this->request->get['since'])){
			try{$since = $this->request->get['since'];}
			catch(Exception $e){}
		}
		$filter_data = array(
			'filter_date_modified'   => $since
		);
		$this->load->model('module/possync');
		$results = $this->model_module_possync->getOrders($filter_data);
		
		$this->response->setOutput(json_encode($results));		

	}

	public function updateQuantity(){
		$this->validateAccess();
		$this->load->model('module/possync');
		$id = -1;
		$qty = -1;
		$sku = null;
		if (isset($this->request->get['id'])){
			try{$id = intval($this->request->get['id']);}
			catch(Exception $e){}
		}
		if ($id>-1){
			if (isset($this->request->get['quantity'])){
				try{$qty = intval($this->request->get['quantity']);}
				catch(Exception $e){}
			}
		}
		if (isset($this->request->get['sku'])){
			try{$sku = $this->request->get['sku'];}
			catch(Exception $e){}
		}		
		$status = false;
		if ($id>-1 && $qty>-1 && strlen($sku)>0){
			$this->model_module_possync->updateQuantity($id, $sku, $qty);
			$status = true;
		}
		$this->response->setOutput($status);
	}
}
?>