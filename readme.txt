POS-Sync Plugin for OpenCart
****************************

Overview
========
The POS-Sync Plugin for OpenCart creates an inventory quantity api for the OpenCart platform.

Installation
============
FTP contents of this zip to the operating directory of your OpenCart server (e.g., /opt/opencart).
All files are unique to this plugin so no files will be overwritten.

Release History
===============
2014-04-23  ver 0.0.2   Added support for getting orders.
2013-05-29	ver 0.0.1	Initial.


Find POS-Sync at https://www.pos-sync.com
Copyright 2013, Nahnay, LLC.